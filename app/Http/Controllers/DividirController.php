<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Stmt\TryCatch;

class DividirController extends Controller
{
    public function dividirNumeros(int $numero1, int $numero2)
    {
        try {
            return $numero1 / $numero2;
        } catch (\Throwable $th) {
            return 'No se puede dividir en 0';
        }

    }
}
