<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MultiplicarController extends Controller
{
    public function multiplicarNumeros($numero1, $numero2)
    {
        return $numero1 * $numero2;
    }
}
