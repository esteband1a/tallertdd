<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RestaController extends Controller
{
    public function restarNumeros(int $numero1, int $numero2)
    {
        
        return $numero1 - $numero2;
    }
}
