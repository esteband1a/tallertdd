<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SumaController extends Controller
{
    public function sumarNumeros($numero1, $numero2)
    {
        if (!$this->validadNumeros($numero1, $numero2)) {
            return 'Se espera valores numericos';
        };

        return $this->sumarDosNumeros($numero1, $numero2);
    }

    public function validadNumeros($numero1, $numero2)
    {

        return is_int($numero1) ? (is_int($numero2) ? true : false) : false;
    }

    public function sumarDosNumeros(int $numero1, int $numero2)
    {
        return $numero1 + $numero2;
    }
}
