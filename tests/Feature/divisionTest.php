<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\DividirController;

class divisionTest extends TestCase
{
    /** @test */
    public function dividir_dos_numeros()
    {
        $objDividir = new DividirController();
        $valorDivision = $objDividir->dividirNumeros(30, 2);
        $this->assertEquals(15, $valorDivision);
    }

    /** @test */
    public function dividir_en_cero()
    {
        $objDividir = new DividirController();
        $valorDivision = $objDividir->dividirNumeros(30, 0);
        $this->assertEquals('No se puede dividir en 0', $valorDivision);
    }
}
