<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\MultiplicarController;

class multiplecaionTest extends TestCase
{
    /** @test */
    public function multiplicar_dos_numeros()
    {
        $objMultiplicar = new MultiplicarController();
        $valorMultiplicacion = $objMultiplicar->multiplicarNumeros(2, 5);
        $this->assertEquals(10, $valorMultiplicacion);
    }
}
