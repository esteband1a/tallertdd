<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\RestaController;

class restaTest extends TestCase
{
    /** @test */
    public function restar_dos_numeros()
    {

        $objResta = new RestaController();
        $valorResta = $objResta->RestarNumeros(10, 7);
        $this->assertEquals(3, $valorResta);
    }
}
