<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\SumaController;

class sumaTest extends TestCase
{

    /** @test */
    public function sumar_dos_cadenas()
    {

        $objSuma = new SumaController();
        $valorSuma = $objSuma->sumarNumeros(3, '2');
        $this->assertEquals('Se espera valores numericos', $valorSuma);
    }

    /** @test */
    public function sumar_dos_numeros()
    {

        $objSuma = new SumaController();
        $valorSuma = $objSuma->sumarNumeros(3, 2);
        $this->assertEquals(5, $valorSuma);
    }

    /** @test */
    public function sumar_dos_negativos()
    {

        $objSuma = new SumaController();
        $valorSuma = $objSuma->sumarNumeros(3, -2);
        $this->assertEquals(1, $valorSuma);
    }
}
